provider "google" {
  credentials = file("juneway-335712-51a94a0932a7.json")
  project     = "juneway-335712"
  region      = "europe-west3"
  zone        = "europe-west3-a"
}


variable "node_count" {
 default = "3"
}


resource "google_compute_instance" "my_servers" {
  count = "${var.node_count}"
  name         = "node${count.index+1}"
  machine_type = "e2-small"
  boot_disk {
    initialize_params {
      size = "20"
      image = "debian-cloud/debian-10"
    }
  }


  network_interface {
    network = "default"
  }
}
